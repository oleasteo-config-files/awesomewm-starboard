local awful = require("awful")

-- table of layouts to iterate through
layouts = {
  awful.layout.suit.tile,
  awful.layout.suit.tile.left,
  awful.layout.suit.tile.bottom,
  awful.layout.suit.tile.top,
  --awful.layout.suit.fair,
  --awful.layout.suit.fair.horizontal,
  --awful.layout.suit.spiral,
  --awful.layout.suit.spiral.dwindle,
  --awful.layout.suit.max,
  --awful.layout.suit.max.fullscreen,
  --awful.layout.suit.magnifier,
  --awful.layout.suit.corner.ne,
  --awful.layout.suit.corner.sw,
  --awful.layout.suit.corner.se,
  awful.layout.suit.floating
}

tag_init = {}

awful.screen.connect_for_each_screen(function (s)
  tag_init[s] = {
    layout = {},
    factor = {}
  }
  for i = 1,tagsAmount do
    tag_init[s].layout[i] = layouts[1]
    tag_init[s].factor[i] = 0
  end

  -- example customization
  tag_init[s].layout[6] = awful.layout.suit.tile.bottom
  tag_init[s].layout[7] = awful.layout.suit.tile
  tag_init[s].factor[7] = 0.2
  tag_init[s].layout[9] = awful.layout.suit.tile.bottom
  tag_init[s].factor[9] = 0.35
end)

