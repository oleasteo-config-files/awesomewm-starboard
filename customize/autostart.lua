local awful = require("awful")
local spawn_once = require("utils/spawn_once")

spawn_once("unclutter --timeout 2")     -- hide cursor when idle
spawn_once("nm-applet")                 -- NetworkManager applet
spawn_once("spacefm -d", {match=true})  -- file-manager daemon
spawn_once("urxvtd")                    -- terminal daemon

awful.spawn("xset -dpms s 0")  -- disable screen auto-off

