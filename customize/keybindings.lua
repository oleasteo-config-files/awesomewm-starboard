local awful = require("awful")
local keys = require("utils/keys")

globalkeys_custom = awful.util.table.join(

  -- --
  -- Display control
  -- --

  awful.key({ keys.super,                       }, "p", function () awful.spawn(bin_dir .. "/screens laptop"); awesome.restart() end, {group="Z: Customize - Display", description="Display laptop"}),
  awful.key({ keys.super, keys.shift            }, "p", function () awful.spawn(bin_dir .. "/screens '='"); awesome.restart()    end, {group="Z: Customize - Display", description="Display laptop = extern (fhd)"}),
  awful.key({ keys.super, keys.alt              }, "p", function () awful.spawn(bin_dir .. "/screens wqhd"); awesome.restart()   end, {group="Z: Customize - Display", description="Display extern (wqhd)"}),
  awful.key({ keys.super, keys.alt,  keys.shift }, "p", function () awful.spawn(bin_dir .. "/screens +wqhd"); awesome.restart()  end, {group="Z: Customize - Display", description="Display laptop left-of extern (wqhd)"}),
  awful.key({ keys.super, keys.ctrl             }, "p", function () awful.spawn(bin_dir .. "/screens fhd"); awesome.restart()    end, {group="Z: Customize - Display", description="Display extern (fhd)"}),
  awful.key({ keys.super, keys.ctrl, keys.shift }, "p", function () awful.spawn(bin_dir .. "/screens +fhd"); awesome.restart()   end, {group="Z: Customize - Display", description="Display laptop left-of extern (fhd)"}),


  -- --
  -- Keyboard backlight
  -- --

--  awful.key({}, keys.kbd_brightness.inc, function () awful.spawn("asus-kbd-backlight up") end, {group="Z: Customize - Keyboard", description="In/decrease keyboard backlight"}),
--  awful.key({}, keys.kbd_brightness.dec, function () awful.spawn("asus-kbd-backlight down") end, {group="Z: Customize - Keyboard", description="In/decrease keyboard backlight"}),


  -- --
  -- Misc Applications
  -- --

  -- Common
  awful.key({ keys.super             }, "e", function () awful.spawn("spacefm -nw") end, {group="Z: Customize - Applications", description="File Manager ~/"}),
  awful.key({ keys.super, keys.shift }, "e", function () awful.spawn("spacefm -nw /data") end, {group="Z: Customize - Applications", description="File Manager /data/"}),
  awful.key({ keys.super             }, "y", function () awful.spawn.with_shell("$HOME/bin/nodekey-terminal") end, {group="Z: Customize - Applications", description="Password Manager"}),
  -- Browsers
  awful.key({ keys.super, keys.alt  }, "f", function () awful.spawn("firefox -P default") end, {group="Z: Customize - Applications", description="Firefox (default profile)"}),
  awful.key({ keys.super, keys.ctrl }, "f", function () awful.spawn("firefox -P focus") end, {group="Z: Customize - Applications", description="Firefox (focus profile)"}),
  -- Messenger
  awful.key({ keys.super, keys.alt }, "g", function () awful.spawn("signal-desktop") end, {group="Z: Customize - Applications", description="Messenger: Signal"}),
  awful.key({ keys.super, keys.alt }, "t", function () awful.spawn("telegram-desktop") end, {group="Z: Customize - Applications", description="Messenger: Telegram"}),
  awful.key({ keys.super, keys.alt }, "e", function () awful.spawn("element-desktop") end, {group="Z: Customize - Applications", description="Messenger: Element"}),
  -- Media
  awful.key({ keys.super }, "a", function () awful.spawn("audacious") end, {group="Z: Customize - Applications", description="Audio Player"}),
  -- Development
  awful.key({ keys.super, keys.alt }, "d", function () awful.spawn("intellij-idea-ultimate-edition") end, {group="Z: Customize - Applications", description="IDE (IntelliJ)"}),
  -- Gaming
  awful.key({ keys.super, keys.alt }, "s", function () awful.spawn.with_shell("LD_PRELOAD='/usr/$LIB/libstdc++.so.6 /usr/$LIB/libgcc_s.so.1 /usr/$LIB/libxcb.so.1 /usr/$LIB/libgpg-error.so' steam")           end, {group="Z: Customize - Applications", description="Steam"}),


  -- --
  -- Application control
  -- --

  -- Audacious
  awful.key({ keys.super, keys.alt }, keys.insert, function () awful.spawn("audtool playback-playpause") end, {group="Z: Customize - Media", description="Toggle Pause"}),
  awful.key({ keys.super, keys.alt }, keys.delete, function () awful.spawn("audtool playlist-reverse") end, {group="Z: Customize - Media", description="Previous/Next Song"}),
  awful.key({ keys.super, keys.alt }, keys.home,   function () awful.spawn("audtool playback-stop") end, {group="Z: Customize - Media", description="Stop"}),
  awful.key({ keys.super, keys.alt }, keys["end"], function () awful.spawn("audtool playlist-advance") end, {group="Z: Customize - Media", description="Previous/Next Song"}),
  awful.key({ keys.super, keys.alt }, keys.down,   function () awful.spawn("audtool playback-playpause") end, {group="Z: Customize - Media", description="Toggle Pause"}),
  awful.key({ keys.super, keys.alt }, keys.left,   function () awful.spawn("audtool playlist-reverse") end, {group="Z: Customize - Media", description="Previous/Next Song"}),
  awful.key({ keys.super, keys.alt }, keys.up,     function () awful.spawn("audtool playback-stop") end, {group="Z: Customize - Media", description="Stop"}),
  awful.key({ keys.super, keys.alt }, keys.right,  function () awful.spawn("audtool playlist-advance") end, {group="Z: Customize - Media", description="Previous/Next Song"})
)


clientkeys_custom = awful.util.table.join(
  -- any additional client-based keybindings (functions receive client as first argument)
)

