-- --
-- General configuration
-- --

tagsAmount = 9           -- the amount of tags to create per screen
sloppyFocus = false      -- focus windows on hover
slaveClient = false      -- start new clients as slave instead of master
desktopHandling = false  -- add mouse-scroll (change tag) and right-click behavior to desktop (open launcher menu)


-- --
-- theme selection
-- --

local theme = "starboard-blue"
local wibox = "wibox-right"
wallpaper = false  -- color or path to wallpaper image
--wallpaper = "/usr/share/webapps/element/themes/element/img/backgrounds/lake.jpg"
--wallpaper = "darkblue"


-- --
-- External Applications
-- --

terminal = "urxvt"
terminal_exec = terminal .. " -e "

editor = "vim"
editor_cmd = terminal_exec .. editor


-- --
-- features
-- set any value to `false` to disable
-- --

traffic_device = "wlan0"       -- see `ip link show
cpu_thermal = "thermal_zone1"  -- see https://github.com/vicious-widgets/vicious#viciouswidgetsthermal
battery = "BAT0"               -- see https://github.com/vicious-widgets/vicious#viciouswidgetsbat
memory = true                  -- show memory usage widget
launcher = false               -- create a simple launcher menu for most basic actions


-- --
-- Derived exports (probably don't want to change those)
-- --

base_dir = string.format("%s/.config/awesome", os.getenv("HOME"))
theme_dir = base_dir .. "/themes/" .. theme
wibox_file = "wiboxes/" .. wibox

