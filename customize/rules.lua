local awful = require("awful")

-- --
-- Define custom rules.
-- For application identification it is easiest to use `xprop` or `xwininfo` and check the WM_CLASS value; The first entry is called "instance", further entries "class" for awesomewm rules.
-- --

customrules = {

  -- move IDEs to tag#1
  {
    rule_any = { class = { "jetbrains-idea", "jetbrains-webstorm", "jetbrains-clion" } },    callback = function(c) if not awesome.startup then c:tags({awful.screen.focused().tags[1]}) end end
  },

  -- move steam to tag#7
  {
    rule = { class = "Steam" },
    properties = { floating = false },
    callback = function(c) if not awesome.startup then c:tags({awful.screen.focused().tags[7]}) end end
  },

  -- move audacious to tag#8
  {
    rule = { instance = "audacious" },
    callback = function(c) if not awesome.startup then c:tags({awful.screen.focused().tags[8]}) end end
  },

  -- move password manager (nodekey) to tag#9 and lowest client
  {
    rule_any = { name = { "nodekey" } },
    properties = { raise = false },
    callback = function(c)
      -- only exact title matches and urxvt class
      if c.class == "URxvt" and c.name == "nodekey" and not awesome.startup then
        c:tags({awful.screen.focused().tags[9]})
        awful.client.setslave(c) 
      end
    end
  },

}

