require("core/debug")
require("awful.autofocus")

require("customize/variables")
require("customize/tags")
require("customize/keybindings")

require("menubar").utils.terminal = terminal
require("beautiful").init(theme_dir .. "/theme.lua")

promptbox = {}
calendars = {}
bin_dir = base_dir .. "/bin"

require("core/tags")
require("core/menu")
require("core/desktop")
require(wibox_file)

local awful = require("awful")
local keybindings = require("core/keybindings")
root.keys(awful.util.table.join(keybindings.globalkeys, globalkeys_custom))

require("core/signals")

require("customize/rules")
awful.rules.rules = awful.util.table.join(require("core/rules"), customrules)

awful.screen.focus(screen.primary)
mouse.coords({x = screen.primary.geometry.width / 2, y = screen.primary.geometry.height / 2})

require("customize/autostart")

