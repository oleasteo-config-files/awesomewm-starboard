local awful = require("awful")
local beautiful = require("beautiful")
local cairo = require("lgi").cairo
local gears = require("gears")

if desktopHandling then

  local buttons = awful.util.table.join(
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
  )

  if launcher then
    buttons = awful.util.table.join(
      buttons,
      awful.button({ }, 3, function () menu:toggle() end)
    )
  end

  root.buttons(buttons)

end

if not wallpaper then wallpaper = "black" end

local wall
if type(wallpaper) == "string" or type(wallpaper) == "function" then
  wall = wallpaper
elseif type(beautiful.wallpaper) == "string" or type(beautiful.wallpaper) == "function" then
  wall = beautiful.wallpaper
end

local function set_wallpaper(s)
  if type(wall) == "function" then wall = wall(s) end

  if not type(wall) == "string" then
    wall = beautiful.wallpaper_color
  end

  if gears.color.parse_color(wall) then
    -- as gears.wallpaper.set does not accept a screen parameter...
    -- gears.wallpaper.set(wall)
    local _, cr = gears.wallpaper.prepare_context(s)
    cr:set_source(gears.color(wall))
    cr:set_operator(cairo.Operator.SOURCE)
    cr:paint()
  else
    gears.wallpaper.maximized(wall, s, true)
  end
end

screen.connect_signal("property::geometry", set_wallpaper)
awful.screen.connect_for_each_screen(set_wallpaper)

