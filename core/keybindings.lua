local awful = require("awful")
local menubar = require("menubar")
local keys = require("utils/keys")
local hotkeys_popup = require("core/hotkeys")

-- TODO add descriptions (everywhere)

local globalkeys = awful.util.table.join(
  -- --
  -- Utilities
  -- --

  awful.key({ keys.super }, keys["return"], function () awful.spawn(terminal) end, {group="0: Utilities", description="Open terminal"}),
  awful.key({ keys.super }, keys.slash, hotkeys_popup.show_help, {group="0: Utilities", description="Show help"}),
  awful.key({ keys.super }, "c", function () calendars[mouse.screen]:toggle() end, {group="0: Utilities", description="Toggle calendar popup"}),
  -- Interactive forced disconnect of clients (useful for irresponsive clients; depends on `xorg-xkill`)
  awful.key({ keys.super }, "x", function () awful.spawn("xkill") end, {group="0: Utilities", description="Select client to kill"}),


  -- --
  -- Media
  -- --

  -- Volume (bin/volume is written for pulseaudio, controls current default output)
  awful.key({      }, keys.volume.inc,  function () awful.spawn(bin_dir .. "/volume + + +") end, {group="8: Media", description="In/decrease volume by 3%"}),
  awful.key({      }, keys.volume.dec,  function () awful.spawn(bin_dir .. "/volume - - -") end, {group="8: Media", description="In/decrease volume by 3%"}),
  awful.key({ keys.ctrl }, keys.volume.inc,  function () awful.spawn(bin_dir .. "/volume +")     end, {group="8: Media", description="In/decrease volume by 1%"}),
  awful.key({ keys.ctrl }, keys.volume.dec,  function () awful.spawn(bin_dir .. "/volume -")     end, {group="8: Media", description="In/decrease volume by 1%"}),
  awful.key({      }, keys.volume.mute, function () awful.spawn(bin_dir .. "/volume m")     end, {group="8: Media", description="Toggle mute"}),
  awful.key({ keys.shift }, keys.volume.inc,  function () awful.spawn(bin_dir .. "/volume =")     end, {group="8: Media", description="Set volume to 100%"}),
  awful.key({ keys.shift }, keys.volume.dec,  function () awful.spawn(bin_dir .. "/volume = 50")     end, {group="8: Media", description="Set volume to 50%"}),
  awful.key({ keys.super, keys.shift }, "Prior", function () awful.spawn(bin_dir .. "/volume =")     end, {group="8: Media", description="Set volume to 100%"}),
  awful.key({ keys.super, keys.shift }, "Next",  function () awful.spawn(bin_dir .. "/volume = 50")     end, {group="8: Media", description="Set volume to 50%"}),
  awful.key({ keys.super       }, "Prior", function () awful.spawn(bin_dir .. "/volume + + +") end, {group="8: Media", description="In/decrease volume by 3%"}),
  awful.key({ keys.super       }, "Next",  function () awful.spawn(bin_dir .. "/volume - - -") end, {group="8: Media", description="In/decrease volume by 3%"}),
  awful.key({ keys.super, keys.alt }, "Prior", function () awful.spawn(bin_dir .. "/volume +")     end, {group="8: Media", description="In/decrease volume by 1%"}),
  awful.key({ keys.super, keys.alt }, "Next",  function () awful.spawn(bin_dir .. "/volume -")     end, {group="8: Media", description="In/decrease volume by 1%"}),
  awful.key({ keys.super       }, "Pause", function () awful.spawn(bin_dir .. "/volume m")     end, {group="8: Media", description="Toggle mute"}),


  -- --
  -- Misc XF86 Keys
  -- --

  -- Calculator (depends on `bc`)
  awful.key({      }, keys.calculator, function () awful.spawn(terminal_exec .. "bc -l") end, {group="A: Misc", description="Open calculator"}),
  -- Touchpad (TODO depends on ...)
--  awful.key({      }, keys.touchpad,   function () awful.spawn("touchpad") end, {group="A: Misc", description="Toggle touchpad disable"}),


  -- --
  -- Display Brightness (depends on `light` and NOPASSWD permissions within /etc/sudoers)
  -- --

  awful.key({      }, keys.brightness.inc, function () awful.spawn("sudo light -A 1") end, {group="9: Backlight", description="In/decrease backlight"}),
  awful.key({      }, keys.brightness.dec, function () awful.spawn("sudo light -U 1") end, {group="9: Backlight", description="In/decrease backlight"}),
  awful.key({ keys.shift }, keys.brightness.inc, function () awful.spawn("sudo light -S 100") end, {group="9: Backlight", description="Set backlight to max/min"}),
  awful.key({ keys.shift }, keys.brightness.dec, function () awful.spawn("sudo light -S 1") end, {group="9: Backlight", description="Set backlight to max/min"}),
  awful.key({ keys.ctrl }, keys.brightness.dec, function () awful.spawn("sudo light -O") end, {group="9: Backlight", description="Save/restore backlight"}),
  awful.key({ keys.ctrl }, keys.brightness.inc, function () awful.spawn("sudo light -I") end, {group="9: Backlight", description="Save/restore backlight"}),


  -- --
  -- Screenshots (depends on `scrot`)
  -- --

  awful.key({      }, keys.print,  function () awful.spawn("scrot")    end, {group="7: Screen", description="Take screenshot"}),
  awful.key({ keys.alt }, keys.print,  function () awful.spawn("scrot -s") end, {group="7: Screen", description="Take screenshot (interactive selection)"}),
  awful.key({ keys.super }, keys.print,  function () awful.spawn("scrot -u") end, {group="7: Screen", description="Take screenshot (focused client)"}),


  -- --
  -- Tag-Navigation
  -- --

  awful.key({ keys.super }, "i", awful.tag.viewnext, {group="5: Tags", description="View next/previous"}),
  awful.key({ keys.super }, "u", awful.tag.viewprev, {group="5: Tags", description="View next/previous"}),


  -- --
  -- Client focus
  -- --

  awful.key({ keys.super }, "j",
    function ()
      awful.client.focus.byidx( 1)
      if client.focus then client.focus:raise() end
    end, {group="4: Client", description="Focus next/previous by index"}),
  awful.key({ keys.super }, "k",
    function ()
      awful.client.focus.byidx(-1)
      if client.focus then client.focus:raise() end
    end, {group="4: Client", description="Focus next/previous by index"}),
  awful.key({ keys.super }, keys.tab,
    function ()
      awful.client.focus.history.previous()
      if client.focus then
        client.focus:raise()
      end
    end, {group="4: Client", description="Focus previous client by focus history"}),


  -- --
  -- Client management
  -- --

  awful.key({ keys.super, keys.shift }, "j", function () awful.client.swap.byidx(  1)    end, {group="4: Client", description="Swap client by index"}),
  awful.key({ keys.super, keys.shift }, "k", function () awful.client.swap.byidx( -1)    end, {group="4: Client", description="Swap client by index"}),
  awful.key({ keys.super, keys.shift }, "n", function () local c = awful.client.restore(); if c then awful.client.focus.byidx(0, c); c:raise() end end, {group="4: Client", description="Restore minimized client"}),
  awful.key({ keys.super, keys.shift }, ",", function () local c = awful.client.restore(); if c then awful.client.focus.byidx(0, c); c:raise() end end, {group="4: Client", description="Restore minimized client"}),


  -- --
  -- Screen focus
  -- --

  awful.key({ keys.super, keys.ctrl }, "j", function () awful.screen.focus_relative( 1) end, {group="7: Screen", description="Focus next/previous"}),
  awful.key({ keys.super, keys.ctrl }, "k", function () awful.screen.focus_relative(-1) end, {group="7: Screen", description="Focus next/previous"}),


  -- --
  -- Layout manipulation
  -- --

  awful.key({ keys.super,      }, "l",      function () awful.tag.incmwfact( 0.05)    end, {group="6: Layout", description="In/decrease master width factor"}),
  awful.key({ keys.super,      }, "h",      function () awful.tag.incmwfact(-0.05)    end, {group="6: Layout", description="In/decrease master width factor"}),
  awful.key({ keys.super, keys.shift }, "h",      function () awful.tag.incnmaster( 1)      end, {group="6: Layout", description="In/decrease master count"}),
  awful.key({ keys.super, keys.shift }, "l",      function () awful.tag.incnmaster(-1)      end, {group="6: Layout", description="In/decrease master count"}),
  awful.key({ keys.super, keys.ctrl }, "h",      function () awful.tag.incncol( 1)         end, {group="6: Layout", description="In/decrease columns"}),
  awful.key({ keys.super, keys.ctrl }, "l",      function () awful.tag.incncol(-1)         end, {group="6: Layout", description="In/decrease columns"}),
  awful.key({ keys.super,      }, keys.space, function () awful.layout.inc(layouts,  1) end, {group="6: Layout", description="Next Layout"}),
  awful.key({ keys.super, keys.shift }, keys.space, function () awful.layout.inc(layouts, -1) end, {group="6: Layout", description="Previous Layout"}),


  -- --
  -- Prompts
  -- --

  awful.key({ keys.super       }, "r", function () promptbox[mouse.screen]:run() end, {group="3: Prompt", description="Grab simple command prompt"}),
  awful.key({ keys.super       }, "grave", function() menubar.show()                 end, {group="3: Prompt", description="Grab advanced command prompt (menubar)"}),
  awful.key({ keys.super, keys.shift }, "r",
    function ()
      awful.prompt.run({
        prompt = "Run Lua code: ",
        textbox = promptbox[mouse.screen].widget,
        exe_callback = awful.util.eval,
        history_path = awful.util.getdir("cache") .. "/history_eval",
      })
    end,
    {group="3: Prompt", description="Grab lua prompt"}),


  -- --
  -- Power Control
  -- --

  awful.key({ keys.super, keys.shift             }, keys.escape, function () awful.spawn("systemctl poweroff") end, {group="1: Power Control", description="Poweroff"}),
  awful.key({ keys.super, keys.shift             }, keys.home,   function () awful.spawn("systemctl reboot")   end, {group="1: Power Control", description="Reboot"}),
  awful.key({ keys.super, keys.shift             }, keys.delete, function () awful.spawn("systemctl suspend")  end, {group="1: Power Control", description="Suspend to RAM"}),


  -- --
  -- Session Control
  -- --

  awful.key({ keys.super, keys.ctrl              }, "r",         awesome.restart, {group="2: Session Control", description="Restart awesome (reload configuration)"}),
  awful.key({ keys.super, keys.ctrl              }, "q",         awesome.quit, {group="2: Session Control", description="Quit awesome (logout)"}),
  -- session lock (depends on `slock`)
  awful.key({ keys.super, keys.ctrl              }, "l",         function () awful.spawn("slock") end, {group="2: Session Control", description="Lock session"}),
  awful.key({ keys.super, keys.ctrl,  keys.shift }, "l",         function () awful.spawn.with_shell("slock systemctl suspend -i") end, {group="2: Session Control", description="Lock session and suspend to RAM"})
)


-- --
-- Digit-based tag access
-- --

for i = 1,tagsAmount do
  globalkeys = awful.util.table.join(
    globalkeys,

    -- jump to tag
    awful.key({ keys.super }, "#" .. i + 9,
      function ()
        local screen = mouse.screen
        local tag = screen.tags[i]
        if tag then
          tag:view_only()
        end
      end,
      {group="5: Tags", description="Jump to tag"}),

    -- toggle tag
    awful.key({ keys.super, keys.ctrl }, "#" .. i + 9,
      function ()
        local screen = mouse.screen
        local tag = screen.tags[i]
        if tag then
          awful.tag.viewtoggle(tag)
        end
      end,
      {group="5: Tags", description="Toggle tag"}),

    -- move client to tag
    awful.key({ keys.super, keys.shift }, "#" .. i + 9,
      function ()
        if client.focus then
          local tag = client.focus.screen.tags[i]
          if tag then
            client.focus:move_to_tag(tag)
          end
        end
      end,
      {group="4: Client", description="Move client to tag"}),

    -- toggle client for tag
    awful.key({ keys.super, keys.ctrl, keys.shift }, "#" .. i + 9,
      function ()
        if client.focus then
          local tag = client.focus.screen.tags[i]
          if tag then
            client.focus:toggle_tag(tag)
          end
        end
      end,
      {group="4: Client", description="Toggle client for tag"})
  )
end

local clientkeys = awful.util.table.join(
  awful.key({ keys.super,      }, "f",       function (c) c.fullscreen = not c.fullscreen; c:raise()  end, {group="B: Focused Client", description="Fullscreen"}),
  awful.key({ keys.super, keys.shift }, "c",       function (c) c:kill()                         end, {group="B: Focused Client", description="Kill"}),
  awful.key({ keys.super, keys.ctrl }, keys.space,  awful.client.floating.toggle, {group="B: Focused Client", description="Toggle forced floating"}                     ),
  awful.key({ keys.super, keys.ctrl }, keys["return"], function (c) c:swap(awful.client.getmaster()) end, {group="B: Focused Client", description="Swap with master"}),
  awful.key({ keys.super,      }, "o",       function (c) c:move_to_screen() end, {group="B: Focused Client", description="Move to next screen"}),
  awful.key({ keys.super,      }, "t",       function (c) c.ontop = not c.ontop            end, {group="B: Focused Client", description="Toggle forced on top"}),
  awful.key({ keys.super,      }, "n",       function (c) c.minimized = true end, {group="B: Focused Client", description="Minimize"}),
  -- FIXME replaced "n" with "," because for some reason "n" does not work (anymore). Also added restore with "," variant.
  awful.key({ keys.super,      }, ",",       function (c) c.minimized = true end, {group="B: Focused Client", description="Minimize"}),
  awful.key({ keys.super,      }, "m",
    function (c)
      c.maximized = not c.maximized
      c.maximized_horizontal = c.maximized
      c.maximized_vertical   = c.maximized
      c:raise()
    end, {group="B: Focused Client", description="Toggle Maximize"})
)

local exports = {
  globalkeys = globalkeys,
  clientkeys = clientkeys
}

return exports

