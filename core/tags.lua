local awful = require("awful")

tags = {}

local tag_names = {}
for i = 1,tagsAmount do
  tag_names[i] = i
end

-- -- register and configure tags
awful.screen.connect_for_each_screen(function(s)
  -- add tags for screen
  tags[s] = awful.tag(tag_names, s, tag_init[s].layout)
  -- incmwfact of given factors
  for i = 1,tagsAmount do
    awful.tag.incmwfact(tag_init[s].factor[i], tags[s][i])
  end
end)

