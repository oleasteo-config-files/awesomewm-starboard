local hotkeys_popup = require("awful.hotkeys_popup.widget")

hotkeys_popup.merge_duplicates = true

return hotkeys_popup

