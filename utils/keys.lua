local keys = {
  super = "Mod4",
  alt = "Mod1",
  ctrl = "Control",
  shift = "Shift",

  backspace = "BackSpace",
  space = "space",
  escape = "Escape",
  home = "Home",
  pause = "Pause",
  tab = "Tab",
  delete = "Delete",
  insert = "Insert",
  print = "Print",
  down = "Down",
  left = "Left",
  up = "Up",
  right = "Right",
  slash = "slash",

  calculator = "XF86Calculator",
  touchpad = "XF86TouchpadToggle",
  volume = {
    inc = "XF86AudioRaiseVolume",
    dec = "XF86AudioLowerVolume",
    mute = "XF86AudioMute",
  },
  brightness = {
    inc = "XF86MonBrightnessUp",
    dec = "XF86MonBrightnessDown",
  },
  kbd_brightness = {
    inc = "XF86KbdBrightnessUp",
    dec = "XF86KbdBrightnessDown",
  }
}
keys["return"] = "Return"
keys["end"] = "End"

return keys

