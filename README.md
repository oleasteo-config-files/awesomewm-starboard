# Starboard

Starboard is my custom built awesome configuration.

Due to its file organization it's not just a theme but rather a configuration bootstrap. It aims to be as distraction-free as appropriate and stay simple yet informative and efficient.

It includes two wibox layouts to choose from, one at the right edge and one at the top. While I prefer the right one (offers more vertical space, better intformation layout), some prefer a top wibox instead.

![wibox-right screenshot](./.screenshots/wibox-right.png)
![wibox-top screenshot](./.screenshots/wibox-top.png)

## Installation

**Prerequisites:** `awesome`, `vicious`

1. Place files at `$HOME/.config/awesome/`.
2. Customize the files within `customize/` to your needs.
   For your first experiences you should enable `launcher` within `customize/variables.lua` for a mouse-based rescue. Don't forget to set `terminal` properly as well.
3. Restart awesome

**Optional** (for some functionality): `pulseaudio`, `xorg-xkill`, `xorg-xrandr`, `light` (see "Display backlight" section), `scrot`, `slock`, `bc`

### Display backlight

For the display backlight to work, `light` needs to be installed. In addition, the user must be able to run `light` with `sudo` without password required (adjust `/etc/sudoers` accordingly).

### Customization

You probably want to adjust everything within the `customize/` directory to your needs. Make sure to not remove any variables completely as it may break some code; Most variables handle `false` quite well.

The checked-in code within `customize/` is a compromise of comments for clarification and my personal configuration.

#### Screens shortcuts

For static XRandR-based keyboard shortcuts you may get adapt `bin/screens` and the respective entries within `customize/keybindings.lua` to your needs.

## Getting started

Press `Super + /` to show keyboard shortcuts.

## Design choices

I use a right-edge wibox because the vertical space is more valuable for me while horizontal space is plenty. Since text is easier to read when rotated by 90deg clockwise, I chose the right side over the left one.

Many keybindings have been persisted from the default awesom configuration. However some have been added/changed in order to improve the workflow. See `Super + /` for a cheat sheet. In addition, power management keybindings have been added that are both safe (you don't hit `Super + Shift + Escape` by accident) and efficient (no need for mouse actions); Those are real quality-of-life improvements for me.

