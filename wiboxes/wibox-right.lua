local awful = require("awful")
local beautiful = require("beautiful")
local wibox = require("wibox")

local separator = wibox.widget.imagebox(beautiful.widget_sep)
local wibar_width = beautiful.wibox_height * 2

awful.screen.connect_for_each_screen(function(s)
  local wibar = awful.wibar({ screen = s, position = "right", width = wibar_width })
  promptbox[s] = require("wiboxes/components/prompt")()

  -- Create top layout
  local layout_top = wibox.layout.fixed.vertical()
  layout_top.spacing = 3
  local layout_top_top = wibox.layout.flex.horizontal()
  require("wiboxes/components/layout")(layout_top_top, s)
  require("wiboxes/components/launcher")(layout_top_top)
  layout_top:add(layout_top_top)
  local datetime_widget = require("wiboxes/components/datetime")(layout_top, s, nil, "<small>%a %y-%m-%d %T</small>")
  datetime_widget.align = "center"

  -- Create left layout
  local layout_tasklist = wibox.layout.align.horizontal()
  layout_tasklist:set_left(separator)
  require("wiboxes/components/tasks")(layout_tasklist, s)
  layout_tasklist:set_right(separator)

  -- Create right layout
  local layout_utils_top = wibox.layout.fixed.horizontal()
  local layout_utils_bot = wibox.layout.fixed.horizontal()
  local layout_utils = wibox.layout.align.horizontal()
  layout_utils_top:add(separator)
  require("wiboxes/components/tags")(layout_utils_top, s)
  layout_utils_top:add(separator)
  layout_utils_top:add(promptbox[s])
  if s.index == 1 then -- systray not working for multiple screens
    layout_utils_bot:add(wibox.widget.systray())
    layout_utils_bot:add(separator)
  end
  if traffic_device then
    require("wiboxes/components/traffic")(layout_utils_bot)
    layout_utils_bot:add(separator)
  end
  if memory then
    require("wiboxes/components/memory")(layout_utils_bot)
    layout_utils_bot:add(separator)
  end
  if cpu_thermal then
    require("wiboxes/components/cpu-temp")(layout_utils_bot)
    layout_utils_bot:add(separator)
  end
  layout_utils:set_first(layout_utils_top)
  layout_utils:set_third(layout_utils_bot)

  -- Combine left and right layout
  local layout_both = wibox.layout.flex.vertical()
  local layout_both_rotated = wibox.container.rotate()
  layout_both:add(layout_utils)
  layout_both:add(layout_tasklist)
  layout_both_rotated:set_direction("west")
  layout_both_rotated:set_widget(layout_both)

  -- Create bottom layout
  local layout_bot = wibox.layout.fixed.vertical()
  layout_bot.spacing = 3
  if battery then
    local battery_widget = require("wiboxes/components/battery")(layout_bot)
    battery_widget.align = "center"
  end
  local volume_widget = require("wiboxes/components/volume")(layout_bot)
  volume_widget.align = "center"

  -- Combine all layouts
  local layout = wibox.layout.align.vertical()
  layout:set_first(layout_top)
  layout:set_second(layout_both_rotated)
  layout:set_third(layout_bot)

  -- Apply layout
  wibar:set_widget(layout)
end)

