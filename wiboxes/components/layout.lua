local awful = require("awful")

local buttons = awful.util.table.join(
  awful.button({ }, 1, function () awful.layout.inc(layouts,  1) end),
  awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
  awful.button({ }, 4, function () awful.layout.inc(layouts,  1) end),
  awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)
)

local function register(layout, screen)
  local widget = awful.widget.layoutbox(screen)
  widget:buttons(buttons)
  
  layout:add(widget)
end

return register

