local wibox = require("wibox")
local vicious = require("vicious")

local function format(_, args)
  local state = args[1]
  local percentage = args[2]

  local fg_color = "white"
  local bg = ""

  if state == "+" then
    bg = "background='darkgreen'"
  elseif state == "-" then
    if percentage <= 10 then
      bg = "background='#ff0000'"
    else
      fg_color = "#cc9955"
    end
  elseif percentage >= 96 then
    fg_color="#888888"
  end

  return "<span color='" .. fg_color .. "' " .. bg .. ">" .. state .. percentage .. "%</span>"
end

local function register(layout)
  local widget = wibox.widget.textbox()
  vicious.register(widget, vicious.widgets.bat, format, 15, battery)

  layout:add(widget)

  return widget
end

return register

