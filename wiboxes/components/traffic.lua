local beautiful = require("beautiful")
local wibox = require("wibox")
local vicious = require("vicious")

local device = traffic_device

local dnicon = wibox.widget.imagebox()
local upicon = wibox.widget.imagebox()
dnicon:set_image(beautiful.widget_net)
upicon:set_image(beautiful.widget_netup)
local widget = wibox.widget.textbox()
vicious.register(
  widget,
  vicious.widgets.net,
  '<span color="' .. beautiful.fg_netdn_widget .. '">${' .. device .. ' down_kb}</span>'
  .. '  '
  .. '<span color="' .. beautiful.fg_netup_widget ..'">${' .. device .. ' up_kb}</span>',
  3
)

local function register(layout)
  layout:add(dnicon)
  layout:add(widget)
  layout:add(upicon)
end

return register

