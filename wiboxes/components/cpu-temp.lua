local wibox = require("wibox")
local beautiful = require("beautiful")
local vicious = require("vicious")

local function register(layout)
  local icon = wibox.widget.imagebox()
  icon:set_image(beautiful.widget_cpu)
  local widget = wibox.widget.textbox()
  vicious.register(widget, vicious.widgets.thermal, " $1 °C", 19, cpu_thermal)

  layout:add(icon)
  layout:add(widget)
end

return register

