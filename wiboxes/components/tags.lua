local awful = require("awful")

local buttons = awful.util.table.join(
  awful.button({      }, 1, awful.tag.viewonly                                         ),
  awful.button({ mkey }, 1, awful.client.movetotag                                     ),
  awful.button({      }, 3, awful.tag.viewtoggle                                       ),
  awful.button({ mkey }, 3, awful.client.toggletag                                     ),
  awful.button({      }, 4, function(t) awful.tag.viewnext(t.screen) end ),
  awful.button({      }, 5, function(t) awful.tag.viewprev(t.screen) end )
)

local function register(layout, screen)
  local widget = awful.widget.taglist(screen, awful.widget.taglist.filter.all, buttons)
  
  layout:add(widget)
end

return register

